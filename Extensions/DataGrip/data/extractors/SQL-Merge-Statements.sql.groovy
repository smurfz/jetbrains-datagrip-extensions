
SEP = ", "
QUOTE     = "\'"
NEWLINE   = System.getProperty("line.separator")

// change to fit current identifying rows

def record(columns, dataRow, keys) {
    OUT.append("MERGE INTO ")
    if (TABLE == null) OUT.append("MY_TABLE")
    else OUT.append(TABLE.getParent().getName()).append(".").append(TABLE.getName())
    OUT.append(" A USING").append(NEWLINE)
    OUT.append("    (SELECT").append(NEWLINE)

//  SELECT

    columns.eachWithIndex { column, idx ->
        def stringValue = FORMATTER.format(dataRow, column)
        def skipQuote = dataRow.value(column).toString().isNumber() || dataRow.value(column) == null
        if (DIALECT.getFamilyId().isMysql()) stringValue = stringValue.replace("\\", "\\\\")
        OUT.append("        ")
        if(stringValue.startsWith("'") && stringValue.endsWith("'")) {
            OUT.append(QUOTE).append(stringValue.substring(1, stringValue.length()-1).replace(QUOTE, QUOTE + QUOTE)).append(QUOTE)
        } else {
            OUT.append(skipQuote ? "" : QUOTE).append(stringValue.replace(QUOTE, QUOTE + QUOTE))
                    .append(skipQuote ? "" : QUOTE)
        }
        OUT.append(" as ")
        OUT.append(column.name()).append(idx != columns.size() - 1 ? SEP : "").append(NEWLINE)
    }
    OUT.append("    FROM DUAL) B").append(NEWLINE)

//  ON

    OUT.append("ON (")
    def result = ""
    columns.eachWithIndex { column, idx ->
        if(keys.contains(column.name())) {
            result += "A." + column.name() + " = B." + column.name() + " and "
        }
    }
    if(result.length() > 5 && result.substring(result.length() - 5, result.length()) == " and ") result = result.substring(0, result.length() - 5)
    OUT.append(result)

//  WHEN NOT MATCHED

    OUT.append(")").append(NEWLINE).append("WHEN NOT MATCHED THEN").append(NEWLINE)

    OUT.append("INSERT (").append(NEWLINE).append("    ")
    columns.eachWithIndex { column, idx ->
        OUT.append(column.name()).append(idx != columns.size() - 1 ? SEP : "")
    }
    OUT.append(")").append(NEWLINE).append("VALUES (").append(NEWLINE).append("    ")
    columns.eachWithIndex { column, idx ->
        OUT.append("B.").append(column.name()).append(idx != columns.size() - 1 ? SEP : "")
    }

//  WHEN MATCHED

    OUT.append(")").append(NEWLINE).append("WHEN MATCHED THEN")
    OUT.append(NEWLINE).append("UPDATE SET").append(NEWLINE)
    columns.eachWithIndex { column, idx ->
        OUT.append("    ").append("A.").append(column.name())
        OUT.append(" = B.").append(column.name()).append(idx != columns.size() - 1 ? SEP : ";").append(NEWLINE)
    }

    OUT.append(NEWLINE)
}

// Add key-columns to compare on to decide if update or insert should be used
ROWS.each { row -> record(COLUMNS, row, ["EXAMPLE_COLUMN_1", "EXAMPLE_COLUMN_2", "EXAMPLE_COLUMN_3"]) }
