= DATAGRIP Extensions

Custom extensions for JetBrains DataGrip

:toc:

---
== 1. Merge statement generator

Custom generator that generates a merge statement (upsert) to insert new records or update existing records based on a match condition. Database management systems Oracle Database, DB2, Teradata, EXASOL, Firebird, CUBRID, HSQLDB, MS SQL, Vectorwise and Apache Derby support this standard syntax.

The generator takes an array of strings which needs to match the columns you wish to base the update on located at the end of the script. Edit the sample values to fit your needs:

[source,groovy]
--
ROWS.each { row -> record(COLUMNS, row, ["EXAMPLE_COLUMN_1", "EXAMPLE_COLUMN_2", "EXAMPLE_COLUMN_3"]) }
--

=== 1.1. Installation

Copy the SQL-Merge-Statements.sql.groovy file to data extractors folder. Default path is <root>\Users\<username>\.DataGrip<version-number>\config\extensions\com.intellij.database\data\extractors but can also be located accessing the `Files` tab in DataGrip and navigating:

Scratches and Consoles -> Extensions -> Database Tools and SQL -> data -> extractors -> right-click folder and `show in Explorer`

